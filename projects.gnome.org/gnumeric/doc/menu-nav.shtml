<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Using Menus</title>
<link rel="previous" href="menu-nav.shtml" title="Using Menus">
<link rel="next" href="menu-bars.shtml" title="Menubar">
<link rel="top" href="gnumeric.shtml" title="The Gnumeric Manual, version 1.12">
<link rel="stylesheet" type="text/css" href="gnumeric-doc.css">
</head>
<body>
<div class="navbar navbar-top"><table class="navbar"><tr>
<td class="navbar-prev"><a class="navbar-prev" href="menu-nav.shtml" title="Using Menus">Using Menus</a></td>
<td class="navbar-next"><a class="navbar-next" href="menu-bars.shtml" title="Menubar">Menubar</a></td>
</tr></table></div>
<div class="sidebar"><div class="sidenav"><div class="autotoc"><ul>
<li><a href="gnumeric-info.shtml" title="About This Document">About This Document</a></li>
<li>
<a class="xref" href="gnumeric.shtml" title="The Gnumeric Manual, version 1.12">The Gnumeric Manual, version 1.12</a><div class="autotoc"><ul>
<li><a class="xref" href="chapter-welcome.shtml" title="Welcome!">Welcome!</a></li>
<li><a class="xref" href="chapter-manual-usage.shtml" title="How to Use This Manual">How to Use This Manual</a></li>
<li><a class="xref" href="chapter-quick-start.shtml" title="A Quick Introduction">A Quick Introduction</a></li>
<li>
<a class="xref" href="chapter-gui-elements.shtml" title="Gnumeric Elements">Gnumeric Elements</a><div class="autotoc"><ul>
<li><a class="xref" href="sect-gui-overview.shtml" title="Overview">Overview</a></li>
<li>
<a class="xref" href="sect-gui-menus.shtml" title="Menus">Menus</a><div class="autotoc"><ul>
<li>Using Menus</li>
<li><a class="xref" href="menu-bars.shtml" title="Menubar">Menubar</a></li>
<li><a class="xref" href="File-Menu.shtml" title="File Menu">File Menu</a></li>
<li><a class="xref" href="Edit-Menu.shtml" title="Edit Menu">Edit Menu</a></li>
<li><a class="xref" href="View-Menu.shtml" title="View Menu">View Menu</a></li>
<li><a class="xref" href="Insert-Menu.shtml" title="Insert Menu">Insert Menu</a></li>
<li><a class="xref" href="Format-Menu.shtml" title="Format Menu">Format Menu</a></li>
<li><a class="xref" href="Tools-Menu.shtml" title="Tools Menu">Tools Menu</a></li>
<li><a class="xref" href="Statistics-Menu.shtml" title="Statistics Menu">Statistics Menu</a></li>
<li><a class="xref" href="Data-Menu.shtml" title="Data Menu">Data Menu</a></li>
<li><a class="xref" href="Help-Menu.shtml" title="Help Menu">Help Menu</a></li>
</ul></div>
</li>
<li><a class="xref" href="context-menu.shtml" title="Context Menus">Context Menus</a></li>
<li><a class="xref" href="sect-gui-toolbars.shtml" title="Toolbars">Toolbars</a></li>
<li><a class="xref" href="data-entry.shtml" title="Data Entry Area">Data Entry Area</a></li>
<li><a class="xref" href="cell-grid.shtml" title="The Cell Grid">The Cell Grid</a></li>
<li><a class="xref" href="info-area.shtml" title="The Information Area">The Information Area</a></li>
<li><a class="xref" href="sect-gui-mouse.shtml" title="The Mouse Pointers used by Gnumeric">The Mouse Pointers used by Gnumeric</a></li>
</ul></div>
</li>
<li><a class="xref" href="chapter-data.shtml" title="Working with Data">Working with Data</a></li>
<li><a class="xref" href="chapter-advanced-analysis.shtml" title="Advanced Analysis">Advanced Analysis</a></li>
<li><a class="xref" href="chapter-solver.shtml" title="The Solver">The Solver</a></li>
<li><a class="xref" href="chapter-stat-analysis.shtml" title="Statistical Analysis">Statistical Analysis</a></li>
<li><a class="xref" href="chapter-graphics.shtml" title="Graphics: Images, Widgets, and Drawings">Graphics: Images, Widgets, and Drawings</a></li>
<li><a class="xref" href="chapter-graphs.shtml" title="Graphs">Graphs</a></li>
<li><a class="xref" href="chapter-worksheets.shtml" title="Using Worksheets">Using Worksheets</a></li>
<li><a class="xref" href="chapter-workbooks.shtml" title="Workbook Settings">Workbook Settings</a></li>
<li><a class="xref" href="chapter-configuring.shtml" title="Configuring Gnumeric">Configuring Gnumeric</a></li>
<li><a class="xref" href="chapter-files.shtml" title="Working with Files">Working with Files</a></li>
<li><a class="xref" href="chapter-printing.shtml" title="Printing">Printing</a></li>
<li><a class="xref" href="chapter-morehelp.shtml" title="Getting More Help">Getting More Help</a></li>
<li><a class="xref" href="chapter-bugs.shtml" title="Reporting a Problem">Reporting a Problem</a></li>
<li><a class="xref" href="chapter-extending.shtml" title="Extending Gnumeric">Extending Gnumeric</a></li>
<li><a class="xref" href="function-reference.shtml" title="Function Reference">Function Reference</a></li>
<li><a class="xref" href="keybinding.shtml" title="Keybinding Reference">Keybinding Reference</a></li>
</ul></div>
</li>
</ul></div></div></div>
<div class="body body-sidebar"><div class="division sect2">
<a name="menu-nav"></a><div class="header"><h1 class="sect2 title"><span class="title">Using Menus</span></h1></div>
<ul class="linktrail">
<li class="linktrail linktrail-first"><a class="linktrail" href="gnumeric.shtml" title="The Gnumeric Manual, version 1.12">The Gnumeric Manual, version 1.12</a></li>
<li class="linktrail"><a class="linktrail" href="chapter-gui-elements.shtml" title="Gnumeric Elements">Gnumeric Elements</a></li>
<li class="linktrail linktrail-last"><a class="linktrail" href="sect-gui-menus.shtml" title="Menus">Menus</a></li>
</ul>
<p class="para block block-first">
    A menu is a graphical element within a program which appears with
    a list of options. For instance, almost all applications have a
    <span class="guimenu">File</span> menu through which the user can access the
    computer's filesystem to open or save their work. The main menus
    are on the menubar. The use of these menus is discussed in <a class="xref" href="menu-bars.shtml" title="Menubar">Section 4.2.2 ― Menubar</a>.
  </p>
<p class="para block">
    <span class="application">Gnumeric</span> also uses context menus to
    give users a quick way to access certain commands. The context
    menu will open up right under the mouse pointer when one of the
    secondary mouse buttons, usually the rightmost, is clicked. This
    menu is called a context menu because the entries in the menu are
    different depending on the location of the mouse pointer. The
    <span class="guilabel">context</span> menus are discussed in <a class="xref" href="context-menu.shtml" title="Context Menus">Section 4.3 ― Context Menus</a>.
  </p>
<p class="para block">
    Both the main menus, on the menubar, and context menus may have
    submenus. A submenu is indicated by a small right-pointing
    arrow. To access a submenu, move the pointer
    down to the submenu entry. When the submenu opens, 
    move the pointer directly across into the submenu.
    When there is not enough room to the right
    of the currently open menu, submenus may open to the
    left. Note that the submenu will
    close if the mouse pointer moves into any other menu entry.
  </p>
<p class="para block">
    You can also use the keyboard to navigate menus and submenus.
    See <a class="xref" href="menu-bars.shtml" title="Menubar">Section 4.2.2 ― Menubar</a> to access the main menus using the keyboard.
    Once a menu is displayed, menu entries can be highlighted by pressing the
    down and up arrow keys. When a submenu opens, pressing the right
    arrow key moves the highlight to the first entry of the submenu. When
    a submenu entry is highlighted, pressing the left arrow key removes
    the highlight from the submenu. Pressing the space bar or the <span class="keycap">Enter</span>
    key activates the highlighted menu entry.
  </p>
<p class="para block">
    Menu entries ending with an ellipsis (three dots) open a dialog window which
    asks for more choices.
  </p>
</div></div>
<div class="navbar navbar-bottom"><table class="navbar"><tr>
<td class="navbar-prev"><a class="navbar-prev" href="menu-nav.shtml" title="Using Menus">Using Menus</a></td>
<td class="navbar-next"><a class="navbar-next" href="menu-bars.shtml" title="Menubar">Menubar</a></td>
</tr></table></div>
</body>
</html>
