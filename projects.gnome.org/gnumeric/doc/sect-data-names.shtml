<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Defining Names</title>
<link rel="previous" href="sect-data-link.shtml" title="Hyperlinks">
<link rel="next" href="chapter-advanced-analysis.shtml" title="Advanced Analysis">
<link rel="top" href="gnumeric.shtml" title="The Gnumeric Manual, version 1.12">
<link rel="stylesheet" type="text/css" href="gnumeric-doc.css">
</head>
<body>
<div class="navbar navbar-top"><table class="navbar"><tr>
<td class="navbar-prev"><a class="navbar-prev" href="sect-data-link.shtml" title="Hyperlinks">Hyperlinks</a></td>
<td class="navbar-next"><a class="navbar-next" href="chapter-advanced-analysis.shtml" title="Advanced Analysis">Advanced Analysis</a></td>
</tr></table></div>
<div class="sidebar"><div class="sidenav"><div class="autotoc"><ul>
<li><a href="gnumeric-info.shtml" title="About This Document">About This Document</a></li>
<li>
<a class="xref" href="gnumeric.shtml" title="The Gnumeric Manual, version 1.12">The Gnumeric Manual, version 1.12</a><div class="autotoc"><ul>
<li><a class="xref" href="chapter-welcome.shtml" title="Welcome!">Welcome!</a></li>
<li><a class="xref" href="chapter-manual-usage.shtml" title="How to Use This Manual">How to Use This Manual</a></li>
<li><a class="xref" href="chapter-quick-start.shtml" title="A Quick Introduction">A Quick Introduction</a></li>
<li><a class="xref" href="chapter-gui-elements.shtml" title="Gnumeric Elements">Gnumeric Elements</a></li>
<li>
<a class="xref" href="chapter-data.shtml" title="Working with Data">Working with Data</a><div class="autotoc"><ul>
<li><a class="xref" href="sect-data-overview.shtml" title="Data in Gnumeric Cells">Data in Gnumeric Cells</a></li>
<li><a class="xref" href="sect-data-types.shtml" title="The Types of Cell Elements">The Types of Cell Elements</a></li>
<li><a class="xref" href="sect-data-entry.shtml" title="Data Entry">Data Entry</a></li>
<li><a class="xref" href="sect-dataentryadv.shtml" title="Advanced Data Entry">Advanced Data Entry</a></li>
<li><a class="xref" href="sect-dataentryextern.shtml" title="Obtaining Data from External Sources">Obtaining Data from External Sources</a></li>
<li><a class="xref" href="sect-data-selections.shtml" title="Selecting Cells and Cell Ranges">Selecting Cells and Cell Ranges</a></li>
<li><a class="xref" href="sect-movecopy.shtml" title="Moving and Copying Data">Moving and Copying Data</a></li>
<li><a class="xref" href="sect-data-delete.shtml" title="Deleting Data">Deleting Data</a></li>
<li><a class="xref" href="sect-data-insert.shtml" title="Inserting New Data Cells">Inserting New Data Cells</a></li>
<li><a class="xref" href="sect-data-format.shtml" title="Formatting Cells">Formatting Cells</a></li>
<li><a class="xref" href="sect-data-format-conditional.shtml" title="Conditional Formatting of Cells">Conditional Formatting of Cells</a></li>
<li><a class="xref" href="sect-data-filter.shtml" title="Filtering Data">Filtering Data</a></li>
<li><a class="xref" href="sect-data-modify.shtml" title="Modifying Data">Modifying Data</a></li>
<li><a class="xref" href="sect-data-generate.shtml" title="Generating Data">Generating Data</a></li>
<li><a class="xref" href="sect-data-comment.shtml" title="Comments in Cells">Comments in Cells</a></li>
<li><a class="xref" href="sect-data-link.shtml" title="Hyperlinks">Hyperlinks</a></li>
<li>Defining Names<div class="autotoc"><ul></ul></div>
</li>
</ul></div>
</li>
<li><a class="xref" href="chapter-advanced-analysis.shtml" title="Advanced Analysis">Advanced Analysis</a></li>
<li><a class="xref" href="chapter-solver.shtml" title="The Solver">The Solver</a></li>
<li><a class="xref" href="chapter-stat-analysis.shtml" title="Statistical Analysis">Statistical Analysis</a></li>
<li><a class="xref" href="chapter-graphics.shtml" title="Graphics: Images, Widgets, and Drawings">Graphics: Images, Widgets, and Drawings</a></li>
<li><a class="xref" href="chapter-graphs.shtml" title="Graphs">Graphs</a></li>
<li><a class="xref" href="chapter-worksheets.shtml" title="Using Worksheets">Using Worksheets</a></li>
<li><a class="xref" href="chapter-workbooks.shtml" title="Workbook Settings">Workbook Settings</a></li>
<li><a class="xref" href="chapter-configuring.shtml" title="Configuring Gnumeric">Configuring Gnumeric</a></li>
<li><a class="xref" href="chapter-files.shtml" title="Working with Files">Working with Files</a></li>
<li><a class="xref" href="chapter-printing.shtml" title="Printing">Printing</a></li>
<li><a class="xref" href="chapter-morehelp.shtml" title="Getting More Help">Getting More Help</a></li>
<li><a class="xref" href="chapter-bugs.shtml" title="Reporting a Problem">Reporting a Problem</a></li>
<li><a class="xref" href="chapter-extending.shtml" title="Extending Gnumeric">Extending Gnumeric</a></li>
<li><a class="xref" href="function-reference.shtml" title="Function Reference">Function Reference</a></li>
<li><a class="xref" href="keybinding.shtml" title="Keybinding Reference">Keybinding Reference</a></li>
</ul></div>
</li>
</ul></div></div></div>
<div class="body body-sidebar"><div class="division sect1">
<a name="sect-data-names"></a><div class="header"><h1 class="sect1 title"><span class="title">Defining Names</span></h1></div>
<ul class="linktrail">
<li class="linktrail linktrail-first"><a class="linktrail" href="gnumeric.shtml" title="The Gnumeric Manual, version 1.12">The Gnumeric Manual, version 1.12</a></li>
<li class="linktrail linktrail-last"><a class="linktrail" href="chapter-data.shtml" title="Working with Data">Working with Data</a></li>
</ul>
<p class="para block block-first">
    Names are labels with a meaning defined in the spreadsheet or by <span class="application">Gnumeric</span>.
    Names can be defined for a whole workbook or for just a single worksheet.
    A name can refer to a numeric value, a string, a range of cells, or a formula.
    A name can be used wherever its meaning could otherwise be used.
  </p>
<p class="para block">
    A name is a sequence of letters, digits, and underscore characters,
    beginning with a letter or underscore.
    A name cannot look like a cell identifier.
    For example, "D7" is not a permitted name, while "D_7" and "_D7" are permitted.
  </p>
<p class="para block">
    To define a name or modify the definition of an existing name:
  </p>
<div class="block list itemizedlist"><ul class="itemizedlist">
<li class="li-first">
      <span class="para">
        If you wish to define or modify a name for use within a single
        worksheet and it is not the current worksheet, click on the
	appropriate worksheet tab at the bottom of the <span class="application">Gnumeric</span> window.
      </span>
    </li>
<li>
      <span class="para">
        Open the <span class="guimenu">Define Names</span> dialog by choosing
        <span class="guimenuitem">Modify ▶ Names</span> from
        the <a class="xref" href="Edit-Menu.shtml" title="Edit Menu">Section 4.2.4 ― Edit Menu</a>.
      </span>
    </li>
<li>
      <span class="para">
        Use the dialog to modify the defined names as desired.
        When your changes are complete, click on <span class="guibutton">Close</span>
        to close the dialog.
      </span>
    </li>
</ul></div>
<div class=" block figure block-indent">
<a name="menu-edit-modify-names.png"></a><div class="block block-first title title-formal"><span class="title"><span class="label"><span style="font-style: italic; ">Figure 5-29</span> </span>The <span class="guimenu">Define Names</span> dialog</span></div>
<div class="figure-inner">
    
    <div class=" block screenshot block-first">
      <div class="mediaobject"><img src="figures/menu-edit-modify-names.png"></div>
    </div>
  </div>
</div>
<p class="para block">
    The <span class="guimenu">Define Names</span> dialog lists the
    names defined in the workbook, organized into two or more groups
    corresponding to the workbook, the active sheet, and the inactive
    sheets, if any. When the dialog is opened, only the names defined
    for the workbook and the active sheet are visible; the definitions
    for any inactive sheets are collapsed into a single line for each,
    identified by the sheet name. The definitions for the workbook or
    a sheet can be exposed or hidden by clicking on the arrow at the
    left end of the workbook or sheet line.
  </p>
<p class="para block">
    The second column of the <span class="guimenu">Define Names</span> dialog can 
    show one of three icons. To define a new name, click on the
    plus sign in the row for the workbook or the active worksheet.
    Names defined under a worksheet name are visible
    only on that worksheet.
    Names defined for the workbook are visible to any
    worksheet that does not have its own definition of the name.
    Names are created as "&lt;new-name&gt;", which must be changed
    before the definition can be used. Click on the new line to select
    it, then click on the name field and type the new name, followed by
    the <span class="keycap">Enter</span> key.
    When you press <span class="keycap">Enter</span>, the name is fixed.
    Only its value can then be changed.
  </p>
<p class="para block">
    If the second column shows a minus sign, the line is for a
    user-defined name. 
    Click on the minus sign to delete the definition of the name.
    If the second column shows a padlock icon, the definition is created 
    automatically and cannot be modified from this dialog.
  </p>
<p class="para block">
    For user-defined names, the third column of the <span class="guimenu">Define
    Names</span> dialog contains a downward- or upward-pointing arrow.
    Click on the arrow to move the definition down to the worksheet or
    up to the workbook.
    This operation is not permitted if the moved definition would replace
    a name already defined for the destination.
    To do such a replacement, first delete the name in the destination,
    then move the definition.
  </p>
<p class="para block">
    The last column of the dialog shows the value defined for the name. 
    When a new name is created, this field is initialized with an absolute 
    reference to the currently selected cells.
    To modify a definition, click on the value field and edit it as
    desired, then press the <span class="keycap">Enter</span> key.
    To define a string value, enclose it in quotation marks, for example,
    'string one' or "string two".
    To specify a range of cells, use absolute references.
    Relative references change in meaning depending on the cell where
    they are used, which is unlikely to produce your intended result.
    You can define a name as a cell or range of cells by clearing its
    existing definition, if any, and then selecting the desired cell or
    cells on the cell grid.
    You can switch worksheets prior to selecting cells.
    The dialog continues to modify names defined for the workbook or the
    worksheet that was displayed when the dialog was opened.
    A reference created by selection in the <span class="guimenu">Define Names</span>
    dialog includes the worksheet name and uses absolute cell coordinates.
    That is, a selection would look like "Sheet1!$A$1:$B$24".
  </p>
<p class="para block">
    Below the display of defined names is a filter box.
    If the workbook or sheet contains many defined names, you can see
    a subset of the names by typing a partial name in the filter box
    and pressing <span class="keycap">Enter</span>.
    All names that do not contain the entered string are omitted from the
    display.
    Differences of case are ignored.
    For example, if “real” were entered in the filter box,
    names such as "Real_Value" and "Surreal" would be displayed.
    When you press <span class="keycap">Enter</span> in the filter box, contexts that contain
    no matching names are automatically collapsed. You may later need 
    to manually expand the workbook line, for example, if the filter
    string is changed.
    If the line for the workbook or a worksheet begins with a
    right-pointing arrow, it contains names that pass the filter.
    Clicking on the arrow will change it to a downward-pointing arrow
    and make the names visible.
    To see all the defined names, clear the filter by clicking on the
    button at the right end of the filter box.
  </p>
<p class="para block">
    In addition to the names defined by the user, <span class="application">Gnumeric</span> has some pre-defined names
    for useful elements:
  </p>
<div class="block list itemizedlist"><ul class="itemizedlist"><li class="li-first">
      <span class="para">
        Sheet_Title: The name of the current sheet.
      </span>
    </li></ul></div>
<div class="block list itemizedlist"><ul class="itemizedlist"><li class="li-first">
      <span class="para">
        Print_Area: The range of cells set as the sheet's
        print area; undefined if print area is not set.
      </span>
    </li></ul></div>
</div></div>
<div class="navbar navbar-bottom"><table class="navbar"><tr>
<td class="navbar-prev"><a class="navbar-prev" href="sect-data-link.shtml" title="Hyperlinks">Hyperlinks</a></td>
<td class="navbar-next"><a class="navbar-next" href="chapter-advanced-analysis.shtml" title="Advanced Analysis">Advanced Analysis</a></td>
</tr></table></div>
</body>
</html>
