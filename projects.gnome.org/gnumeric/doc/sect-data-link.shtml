<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Hyperlinks</title>
<link rel="previous" href="sect-data-comment.shtml" title="Comments in Cells">
<link rel="next" href="sect-data-names.shtml" title="Defining Names">
<link rel="top" href="gnumeric.shtml" title="The Gnumeric Manual, version 1.12">
<link rel="stylesheet" type="text/css" href="gnumeric-doc.css">
</head>
<body>
<div class="navbar navbar-top"><table class="navbar"><tr>
<td class="navbar-prev"><a class="navbar-prev" href="sect-data-comment.shtml" title="Comments in Cells">Comments in Cells</a></td>
<td class="navbar-next"><a class="navbar-next" href="sect-data-names.shtml" title="Defining Names">Defining Names</a></td>
</tr></table></div>
<div class="sidebar"><div class="sidenav"><div class="autotoc"><ul>
<li><a href="gnumeric-info.shtml" title="About This Document">About This Document</a></li>
<li>
<a class="xref" href="gnumeric.shtml" title="The Gnumeric Manual, version 1.12">The Gnumeric Manual, version 1.12</a><div class="autotoc"><ul>
<li><a class="xref" href="chapter-welcome.shtml" title="Welcome!">Welcome!</a></li>
<li><a class="xref" href="chapter-manual-usage.shtml" title="How to Use This Manual">How to Use This Manual</a></li>
<li><a class="xref" href="chapter-quick-start.shtml" title="A Quick Introduction">A Quick Introduction</a></li>
<li><a class="xref" href="chapter-gui-elements.shtml" title="Gnumeric Elements">Gnumeric Elements</a></li>
<li>
<a class="xref" href="chapter-data.shtml" title="Working with Data">Working with Data</a><div class="autotoc"><ul>
<li><a class="xref" href="sect-data-overview.shtml" title="Data in Gnumeric Cells">Data in Gnumeric Cells</a></li>
<li><a class="xref" href="sect-data-types.shtml" title="The Types of Cell Elements">The Types of Cell Elements</a></li>
<li><a class="xref" href="sect-data-entry.shtml" title="Data Entry">Data Entry</a></li>
<li><a class="xref" href="sect-dataentryadv.shtml" title="Advanced Data Entry">Advanced Data Entry</a></li>
<li><a class="xref" href="sect-dataentryextern.shtml" title="Obtaining Data from External Sources">Obtaining Data from External Sources</a></li>
<li><a class="xref" href="sect-data-selections.shtml" title="Selecting Cells and Cell Ranges">Selecting Cells and Cell Ranges</a></li>
<li><a class="xref" href="sect-movecopy.shtml" title="Moving and Copying Data">Moving and Copying Data</a></li>
<li><a class="xref" href="sect-data-delete.shtml" title="Deleting Data">Deleting Data</a></li>
<li><a class="xref" href="sect-data-insert.shtml" title="Inserting New Data Cells">Inserting New Data Cells</a></li>
<li><a class="xref" href="sect-data-format.shtml" title="Formatting Cells">Formatting Cells</a></li>
<li><a class="xref" href="sect-data-format-conditional.shtml" title="Conditional Formatting of Cells">Conditional Formatting of Cells</a></li>
<li><a class="xref" href="sect-data-filter.shtml" title="Filtering Data">Filtering Data</a></li>
<li><a class="xref" href="sect-data-modify.shtml" title="Modifying Data">Modifying Data</a></li>
<li><a class="xref" href="sect-data-generate.shtml" title="Generating Data">Generating Data</a></li>
<li><a class="xref" href="sect-data-comment.shtml" title="Comments in Cells">Comments in Cells</a></li>
<li>Hyperlinks<div class="autotoc"><ul></ul></div>
</li>
<li><a class="xref" href="sect-data-names.shtml" title="Defining Names">Defining Names</a></li>
</ul></div>
</li>
<li><a class="xref" href="chapter-advanced-analysis.shtml" title="Advanced Analysis">Advanced Analysis</a></li>
<li><a class="xref" href="chapter-solver.shtml" title="The Solver">The Solver</a></li>
<li><a class="xref" href="chapter-stat-analysis.shtml" title="Statistical Analysis">Statistical Analysis</a></li>
<li><a class="xref" href="chapter-graphics.shtml" title="Graphics: Images, Widgets, and Drawings">Graphics: Images, Widgets, and Drawings</a></li>
<li><a class="xref" href="chapter-graphs.shtml" title="Graphs">Graphs</a></li>
<li><a class="xref" href="chapter-worksheets.shtml" title="Using Worksheets">Using Worksheets</a></li>
<li><a class="xref" href="chapter-workbooks.shtml" title="Workbook Settings">Workbook Settings</a></li>
<li><a class="xref" href="chapter-configuring.shtml" title="Configuring Gnumeric">Configuring Gnumeric</a></li>
<li><a class="xref" href="chapter-files.shtml" title="Working with Files">Working with Files</a></li>
<li><a class="xref" href="chapter-printing.shtml" title="Printing">Printing</a></li>
<li><a class="xref" href="chapter-morehelp.shtml" title="Getting More Help">Getting More Help</a></li>
<li><a class="xref" href="chapter-bugs.shtml" title="Reporting a Problem">Reporting a Problem</a></li>
<li><a class="xref" href="chapter-extending.shtml" title="Extending Gnumeric">Extending Gnumeric</a></li>
<li><a class="xref" href="function-reference.shtml" title="Function Reference">Function Reference</a></li>
<li><a class="xref" href="keybinding.shtml" title="Keybinding Reference">Keybinding Reference</a></li>
</ul></div>
</li>
</ul></div></div></div>
<div class="body body-sidebar"><div class="division sect1">
<a name="sect-data-link"></a><div class="header"><h1 class="sect1 title"><span class="title">Hyperlinks</span></h1></div>
<ul class="linktrail">
<li class="linktrail linktrail-first"><a class="linktrail" href="gnumeric.shtml" title="The Gnumeric Manual, version 1.12">The Gnumeric Manual, version 1.12</a></li>
<li class="linktrail linktrail-last"><a class="linktrail" href="chapter-data.shtml" title="Working with Data">Working with Data</a></li>
</ul>
<p class="para block block-first">
    Each cell can have an associated hyperlink.
    A hyperlink permits the user of a spreadsheet to go directly to
    a particular cell or group of cells, to access another file on
    the local computer or on the web, or to send an email message
    to an address built into the link.
  </p>
<p class="para block">
    To add a hyperlink to one or more cells, first select the cells.
    Next choose <span class="guimenuitem">Hyperlink...</span>
    from the <a class="xref" href="Insert-Menu.shtml" title="Insert Menu">Section 4.2.6 ― Insert Menu</a> to open the <span class="guimenu">HyperLink</span> dialog.
  </p>
<div class=" block figure block-indent">
<a name="menu-insert-hyperlink.png"></a><div class="block block-first title title-formal"><span class="title"><span class="label"><span style="font-style: italic; ">Figure 5-28</span> </span>The <span class="guimenu">HyperLink</span> dialog.</span></div>
<div class="figure-inner">
    
    <div class=" block screenshot block-first">
      <div class="mediaobject"><img src="figures/menu-insert-hyperlink.png"></div>
    </div>
  </div>
</div>
<p class="para block">
    If the selection includes a hyperlink, the dialog is initialized from
    the existing link.
    If there is more than one hyperlink in the selection, the cell in
    the first row that has a hyperlink, and the first such column of that
    row, is used to initialize the dialog.
  </p>
<p class="para block">
    When you click on <span class="guibutton">OK</span>, the information in the
    dialog is attached to each cell in the selection, replacing any
    previously defined links.
    For any empty cells in the selection, the text of the link is set as
    the contents of the cell.
    For all cells in the selection, the cell format is modified to give
    the contents a distinctive color, and the text is underlined.
  </p>
<p class="para block">
    If the link location text box is empty when you click on <span class="guibutton">OK</span>, 
    no hyperlink is created.
    Instead, if any cells in the selection have existing hyperlinks, those
    hyperlinks are removed.
    The distinctive format applied to links is not removed.
    For that reason it might be preferrable to choose
    <a class="xref" href="Edit-Menu.shtml#edit-menu-clear-links" title="Edit Menu">Clear ▶ Formats &amp; Hyperlinks</a>
    from the <a class="xref" href="Edit-Menu.shtml" title="Edit Menu">Section 4.2.4 ― Edit Menu</a> to remove existing hyperlinks.
  </p>
<p class="para block">
    <span class="application">Gnumeric</span> supports four types of hyperlink.
    The first element of the <span class="guimenu">HyperLink</span> dialog, the
    <span class="guimenu">Type</span> menu, selects the type of hyperlink to
    be created.
    The <span class="guimenu">HyperLink</span> dialog varies slightly, depending
    on the selected hyperlink type:
    <div class="block list itemizedlist"><ul class="itemizedlist">
<li class="li-first">
        <p class="para block block-first">
          Internal Link: The second element of the dialog is a text box
          labeled "Target Range". You can enter a single range, with or 
          without a worksheet name.
          To select the range from the worksheet, click on the button
          at the end of the "Target Range" line. This collapses the
          dialog to a single line and makes it possible to interact with
          the cell grid. Select the desired sheet tab, if necessary,
          then select the desired cells.
          When the selection is complete, click on the button at the
          end of the Target Range input box to open the full
          <span class="guimenu">HyperLink</span> dialog again.
        </p>
        <p class="para block">
          When an internal link is activated by clicking on the cell,
          the range of cells given by the link address
          becomes the current selection. The cell closest to A1 in the
          new selection becomes the active cell.
        </p>
      </li>
<li>
        <p class="para block block-first">
          External Link: The second element of the dialog is a text box
          labeled "File". Enter the path to a file. You can also enter
          a Universal Resource Locator (URL) here; the URL is accessed
          as described for Web Link below.
        </p>
        <p class="para block">
          When an external link is activated by clicking on the cell,
          <span class="application">Gnumeric</span> launches an application to present the file,
          based on its apparent type. For example, "mypic.jpg" would
          be opened in an image viewer, while "myinfo.html" would be
          opened in a web browser.
        </p>
      </li>
<li>
        <p class="para block block-first">
          Email Link: The dialog provides text boxes where you can
          enter the destination address and subject line for an e-mail message.
        </p>
        <p class="para block">
          When an email link is activated by clicking on the cell,
          <span class="application">Gnumeric</span> launches an e-mail client to send a message. The message
          is initialized with the destination address and
          subject specified in the dialog.
        </p>
      </li>
<li>
        <p class="para block block-first">
          Web Link: Enter a Universal Resource Locator (URL) in the
          Web Address text box.
        </p>
        <p class="para block">
          When the link is activated by clicking on the cell,
          <span class="application">Gnumeric</span> launches an application to display the information
          at the specified URL, based on its apparent type. For a
          web address beginning with "http://" or "https://" the application is
          the default web browser.
        </p>
      </li>
</ul></div>
  </p>
<p class="para block">
    When you move the mouse pointer to a hyperlink in the cell grid,
    a tool tip appears.
    The default tip shows the link address and instructions for
    activating the link or selecting the cell.
    To define a tip specific to the link, select the radio button
    next to "Tip", then enter the tip text in the text box.
    If the link-specific tip is selected and left blank, no tip is displayed
    when the mouse pointer is on the link.
  </p>
</div></div>
<div class="navbar navbar-bottom"><table class="navbar"><tr>
<td class="navbar-prev"><a class="navbar-prev" href="sect-data-comment.shtml" title="Comments in Cells">Comments in Cells</a></td>
<td class="navbar-next"><a class="navbar-next" href="sect-data-names.shtml" title="Defining Names">Defining Names</a></td>
</tr></table></div>
</body>
</html>
